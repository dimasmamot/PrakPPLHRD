<?php
	$this->title = 'Status Surat';
?>

<h1>Status Surat Pegawai Klinik Strong</h1>

<hr>

<table class="table table-bordered table-striped">
	<thread>
		<tr>
			<td>Nomor Surat</td>
			<td>Jenis Surat</td>
			<td>Tanggal Permintaan</td>
			<td>Status</td>
		</tr>
	</thread>
	<tbody>
		<?php foreach ($query as $team): ?>
			<tr>
				<td><?= $team['id_surat'] ?></td>
				<td><?= $team['jenis_surat'] ?></td>
				<td><?= $team['tanggal_request'] ?></td>
				<td><?= $team['status_acc'] ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

