<?php
	$this->title = 'Posisi Kerja';
?>

<h1>Keterangan Posisi Pegawai Klinik Strong</h1>

<hr>

<table class="table table-bordered table-striped">
	<thread>
		<tr>
			<td>First Name</td>
			<td>Last Name</td>
			<td>Username</td>
			<td>Posisi</td>
			<td>Departemen</td>
			<td>Kota Kerja</td>
		</tr>
	</thread>
	<tbody>
		<?php foreach ($query as $team): ?>
			<tr>
				<td><?= $team['fname'] ?></td>
				<td><?= $team['lname'] ?></td>
				<td><?= $team['username'] ?></td>
				<td><?= $team['nama_posisi'] ?></td>
				<td><?= $team['departemen'] ?></td>
				<td><?= $team['kota'] ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

