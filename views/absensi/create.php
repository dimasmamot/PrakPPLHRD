<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Absensi */

$this->title = 'Create Absensi';
$this->params['breadcrumbs'][] = ['label' => 'Index Absensi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="absensi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
