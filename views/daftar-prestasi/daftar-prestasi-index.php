<?php
	$this->title = 'Daftar Prestasi';
?>

<h1>Daftar Prestasi Pegawai Klinik Strong</h1>

<hr>

<table class="table table-bordered table-striped">
	<thread>
		<tr>
			<td>First Name</td>
			<td>Last Name</td>
			<td>Username</td>
			<td>Nama Prestasi</td>
			<td>Tahun</td>
			<td>Tingkat</td>
		</tr>
	</thread>
	<tbody>
		<?php foreach ($query as $team): ?>
			<tr>
				<td><?= $team['fname'] ?></td>
				<td><?= $team['lname'] ?></td>
				<td><?= $team['username'] ?></td>
				<td><?= $team['nama_prestasi'] ?></td>
				<td><?= $team['tahun_prestasi'] ?></td>
				<td><?= $team['tingkat_prestasi'] ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

