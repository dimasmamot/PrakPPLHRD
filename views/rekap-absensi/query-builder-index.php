<?php
	$this->title = 'Rekap Absensi';
?>

<h1>Rekap Absensi Pegawai Klinik Strong</h1>

<hr>

<table class="table table-bordered table-striped">
	<thread>
		<tr>
			<td>First Name</td>
			<td>Last Name</td>
			<td>Username</td>
			<td>Waktu Absen</td>
			<td>Status Absen</td>
		</tr>
	</thread>
	<tbody>
		<?php foreach ($query as $team): ?>
			<tr>
				<td><?= $team['fname'] ?></td>
				<td><?= $team['lname'] ?></td>
				<td><?= $team['username'] ?></td>
				<td><?= $team['waktu_absen'] ?></td>
				<td><?= $team['status_absen'] ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

