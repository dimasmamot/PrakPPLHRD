<?php
	$this->title = 'Jadwal Lembur';
?>

<h1>Jadwal Lembur Pegawai Klinik Strong</h1>

<hr>

<table class="table table-bordered table-striped">
	<thread>
		<tr>
			<td>First Name</td>
			<td>Last Name</td>
			<td>Username</td>
			<td>Upah Lembur</td>
			<td>AktivitasLembur</td>
			<td>Waktu Lembur (Jam/Hari)</td>
		</tr>
	</thread>
	<tbody>
		<?php foreach ($query as $team): ?>
			<tr>
				<td><?= $team['fname'] ?></td>
				<td><?= $team['lname'] ?></td>
				<td><?= $team['username'] ?></td>
				<td><?= $team['upah_lembur'] ?></td>
				<td><?= $team['aktivitas_lembur'] ?></td>
				<td><?= $team['waktu_lembur'] ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

