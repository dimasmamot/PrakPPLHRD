<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Memo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="memo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'judul_memo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isi_memo')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status_memo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
