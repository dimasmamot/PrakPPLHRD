<?php
	$this->title = 'Lihat Memo';
?>

<h1>Lihat Memo Klinik Strong</h1>

<hr>

<table class="table table-bordered table-striped">
	<thread>
		<tr>
			<td>Nomor Memo</td>
			<td>Judul Memo</td>
			<td>Isi Memo</td>
			<td>Status Memo</td>
		</tr>
	</thread>
	<tbody>
		<?php foreach ($query as $team): ?>
			<tr>
				<td><?= $team['id_memo'] ?></td>
				<td><?= $team['judul_memo'] ?></td>
				<td><?= $team['isi_memo'] ?></td>
				<td><?= $team['status_memo'] ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

