<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SuratSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="surat-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_surat') ?>

    <?= $form->field($model, 'id_pegawai') ?>

    <?= $form->field($model, 'jenis_surat') ?>

    <?= $form->field($model, 'tanggal_request') ?>

    <?= $form->field($model, 'status_acc') ?>

    <?php // echo $form->field($model, 'tanggal_acc') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
