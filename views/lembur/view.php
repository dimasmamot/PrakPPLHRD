<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Lembur */

$this->title = $model->id_lembur;
$this->params['breadcrumbs'][] = ['label' => 'Index Lembur', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembur-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_lembur], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_lembur], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_lembur',
            'id_pegawai',
            'upah_lembur',
            'aktivitas_lembur',
            'waktu_lembur',
        ],
    ]) ?>

</div>
