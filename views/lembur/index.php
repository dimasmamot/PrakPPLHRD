<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LemburSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Index Lembur';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lembur-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Lembur', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_lembur',
            'id_pegawai',
            'upah_lembur',
            'aktivitas_lembur',
            'waktu_lembur',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
