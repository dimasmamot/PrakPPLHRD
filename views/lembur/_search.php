<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LemburSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lembur-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_lembur') ?>

    <?= $form->field($model, 'id_pegawai') ?>

    <?= $form->field($model, 'upah_lembur') ?>

    <?= $form->field($model, 'aktivitas_lembur') ?>

    <?= $form->field($model, 'waktu_lembur') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
