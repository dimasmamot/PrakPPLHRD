<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Pegawai;

/* @var $this yii\web\View */
/* @var $model app\models\Posisi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posisi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_pegawai')->dropDownList(
    	ArrayHelper::map(Pegawai::find()->all(),'id_pegawai','fname'),['prompt'=>'Pilih Pegawai']
    	) 
    ?>

    <?= $form->field($model, 'nama_posisi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'departemen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kota')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
