<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Posisi */

$this->title = 'Update Posisi: ' . $model->id_posisi;
$this->params['breadcrumbs'][] = ['label' => 'Index Posisi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_posisi, 'url' => ['view', 'id' => $model->id_posisi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="posisi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
