<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PosisiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Index Posisi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posisi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Posisi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_posisi',
            'id_pegawai',
            'nama_posisi',
            'departemen',
            'kota',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
