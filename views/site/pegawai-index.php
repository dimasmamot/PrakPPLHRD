<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
$idpeg = Yii::$app->user->identity->id_pegawai;
$this->title = 'Dashboard Pegawai';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>STRONG HRD - PEGAWAI</h1>

        <p class="lead">Lihat Data Diri Sekarang!</p>

        <p><?= Html::a('Lihat Data Diri', ['/pegawai/view', 'id' => $idpeg ],['class'=>'btn btn-lg btn-success'])?></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Absensi</h2>

                <p>Lakukan Absensi</p>

                <p><?= Html::a('Absen &raquo;', ['/submit-absensi'],['class'=>'btn btn-primary'])?></p>
            </div>

            <div class="col-lg-4">
                <h2>Posisi</h2>

                <p>Lihat Keterangan Posisi</p>

                <p><?= Html::a('Lihat Posisi', ['/lihat-posisi', 'id' => $idpeg ],['class'=>'btn btn-success'])?></p>
            </div>

            <div class="col-lg-4">
                <h2>Prestasi</h2>

                <p>Lihat Prestasi</p>

                <p><?= Html::a('Lihat Daftar Prestasi', ['/daftar-prestasi', 'id' => $idpeg ],['class'=>'btn btn-info'])?></p>
            </div>

            <div class="col-lg-4">
                <h2>Lihat Jadwal Lembur</h2>

                <p>Lihat Jadwal Lembur Anda</p>

                <p><?= Html::a('Jadwal Lembur &raquo;', ['/jadwal-lembur'],['class'=>'btn btn-warning'])?></p>
            </div>

            <div class="col-lg-4">
                <h2>Memo</h2>

                <p>Lihat Memo yang Sedang Aktif</p>

                <p><?= Html::a('Lihat Memo &raquo;', ['/lihat-memo'],['class'=>'btn btn-warning'])?></p>
            </div>

            <div class="col-lg-4">
                <h2>Request Surat</h2>

                <p>Buat Permohonan Surat</p>

                <p><?= Html::a('Request Surat Ijin &raquo;', ['/submit-surat/surat-ijin'],['class'=>'btn btn-info'])?></p>
                <p><?= Html::a('Request Surat Cuti &raquo;', ['/submit-surat/surat-cuti'],['class'=>'btn btn-warning'])?></p>
                <p><?= Html::a('Request Surat Jalan &raquo;', ['/submit-surat/surat-jalan'],['class'=>'btn btn-primary'])?></p>
                <p><?= Html::a('Request Surat Keterangan Gaji &raquo;', ['/submit-surat/surat-gaji'],['class'=>'btn btn-success'])?></p>
                <p><?= Html::a('Request Surat Pengunduran diri &raquo;', ['/submit-surat/surat-pengunduran'],['class'=>'btn btn-warning'])?></p>
            </div>

            <div class="col-lg-4">
                <h2>Lihat Status Surat</h2>

                <p>Lihat Status Surat yang Telah Diajukan</p>

                <p><?= Html::a('Lihat Status Surat &raquo;', ['/lihat-surat'],['class'=>'btn btn-info'])?></p>
            </div>
        </div>

    </div>
</div>
