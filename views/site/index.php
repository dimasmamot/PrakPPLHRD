<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Dashboard Petinggi';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>STRONG HRD - DIREKSI</h1>

        <p class="lead">Buat Pegawai Baru Sekarang !</p>

        <p><?= Html::a('Buat Pegawai Baru', ['/pegawai/create'],['class'=>'btn btn-lg btn-success'])?></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Assign Posisi</h2>

                <p>Cek dan assign posisi dan peran pegawai</p>

                <p><?= Html::a('Lihat Absensi &raquo;', ['/posisi/create'],['class'=>'btn btn-info'])?></p>
            </div>

            <div class="col-lg-4">
                <h2>Absensi</h2>

                <p>Cek rekap absensi pegawai, dan atur absensi pegawai</p>

                <p><?= Html::a('Lihat Absensi &raquo;', ['/rekap-absensi'],['class'=>'btn btn-primary'])?></p>
            </div>

            <div class="col-lg-4">
                <h2>Assign Jadwal Lembur</h2>

                <p>Assign jadwal lembur pegawai, dan atur pengaturan lemburnya</p>

                <p><?= Html::a('Jadwal Lembur &raquo;', ['/lembur/create'],['class'=>'btn btn-warning'])?></p>
            </div>

            <div class="col-lg-4">
                <h2>Memo</h2>

                <p>Buat memo untuk disampaikan ke pegawai yang lain secara instan</p>

                <p><?= Html::a('Buat Memo &raquo;', ['/memo/create'],['class'=>'btn btn-warning'])?></p>
            </div>

            <div class="col-lg-4">
                <h2>Prestasi Pegawai</h2>

                <p>Tambah detail prestasi para pegawai</p>

                <p><?= Html::a('Prestasi Pegawai &raquo;', ['/prestasi/create'],['class'=>'btn btn-primary'])?></p>
            </div>

            <div class="col-lg-4">
                <h2>Surat</h2>

                <p>Atur dan ubah status surat (Khusus Manajer)</p>

                <p><?= Html::a('ACC Surat &raquo;', ['/surat/index'],['class'=>'btn btn-info'])?></p>
            </div>
        </div>

    </div>
</div>
