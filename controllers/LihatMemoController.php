<?php
	namespace app\controllers;

	use Yii;
	use yii\filters\AccessControl;
	use yii\web\Controller;

	class LihatMemoController extends Controller{
		public function actions(){
			return[
				'error' => [
					'class' => 'yii\web\ErrorAction',
				],
			];
		}

		public function actionIndex(){
			$query = (new \yii\db\Query())
						->from('memo')
						->limit(5)
						->all();

			return $this->render('lihat-memo-index', [
					'query'=>$query
				]);
		}
	}
?>
