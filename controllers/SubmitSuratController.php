<?php
	namespace app\controllers;

	use Yii;
	use yii\filters\AccessControl;
	use yii\web\Controller;
	use yii\db\QueryBuilder;

	class SubmitSuratController extends Controller{		
		public function actions(){
			return[
				'error' => [
					'class' => 'yii\web\ErrorAction',
				],
			];
		}

		public function actionIndex(){
			
		}

		public function actionSuratIjin(){
			$sql = Yii::$app->db->createCommand()
					->insert('surat', 
						[
						'id_pegawai' => Yii::$app->user->identity->id_pegawai,
						'jenis_surat' => 'Surat Ijin'
					])->execute();

			return $this->render('submit-surat-index');
		}

		public function actionSuratCuti(){
			$sql = Yii::$app->db->createCommand()
					->insert('surat', 
						[
						'id_pegawai' => Yii::$app->user->identity->id_pegawai,
						'jenis_surat' => 'Surat Cuti'
					])->execute();

			return $this->render('submit-surat-index');
		}

		public function actionSuratJalan(){
			$sql = Yii::$app->db->createCommand()
					->insert('surat', 
						[
						'id_pegawai' => Yii::$app->user->identity->id_pegawai,
						'jenis_surat' => 'Surat Jalan'
					])->execute();

			return $this->render('submit-surat-index');
		}

		public function actionSuratGaji(){
			$sql = Yii::$app->db->createCommand()
					->insert('surat', 
						[
						'id_pegawai' => Yii::$app->user->identity->id_pegawai,
						'jenis_surat' => 'Surat Keterangan Gaji'
					])->execute();

			return $this->render('submit-surat-index');
		}

		public function actionSuratPengunduran(){
			$sql = Yii::$app->db->createCommand()
					->insert('surat', 
						[
						'id_pegawai' => Yii::$app->user->identity->id_pegawai,
						'jenis_surat' => 'Surat Pengunduran Diri'
					])->execute();

			return $this->render('submit-surat-index');
		}
	}
?>
