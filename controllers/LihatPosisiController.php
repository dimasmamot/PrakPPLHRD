<?php
	namespace app\controllers;

	use Yii;
	use yii\filters\AccessControl;
	use yii\web\Controller;

	class LihatPosisiController extends Controller{
		public function actions(){
			return[
				'error' => [
					'class' => 'yii\web\ErrorAction',
				],
			];
		}

		public function actionIndex(){
			$query = (new \yii\db\Query())
						->select(['pegawai.fname','pegawai.lname','pegawai.username','posisi.nama_posisi','posisi.departemen',
							'posisi.kota'])
						->from('posisi')
						->join('INNER JOIN', 'pegawai', 'posisi.id_pegawai = pegawai.id_pegawai')
						->limit(5)
						->all();

			return $this->render('lihat-posisi-index', [
					'query'=>$query
				]);
		}
	}
?>
