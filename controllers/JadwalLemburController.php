<?php
	namespace app\controllers;

	use Yii;
	use yii\filters\AccessControl;
	use yii\web\Controller;

	class JadwalLemburController extends Controller{
		public function actions(){
			return[
				'error' => [
					'class' => 'yii\web\ErrorAction',
				],
			];
		}

		public function actionIndex(){
			$query = (new \yii\db\Query())
						->select(['pegawai.fname','pegawai.lname','pegawai.username','lembur.upah_lembur','lembur.aktivitas_lembur',
							'lembur.waktu_lembur'])
						->from('lembur')
						->join('INNER JOIN', 'pegawai', 'lembur.id_pegawai = pegawai.id_pegawai')
						->limit(5)
						->all();

			return $this->render('jadwal-lembur-index', [
					'query'=>$query
				]);
		}
	}
?>
