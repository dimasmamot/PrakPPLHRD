<?php
	namespace app\controllers;

	use Yii;
	use yii\filters\AccessControl;
	use yii\web\Controller;

	class DaftarPrestasiController extends Controller{
		public function actions(){
			return[
				'error' => [
					'class' => 'yii\web\ErrorAction',
				],
			];
		}

		public function actionIndex(){
			$query = (new \yii\db\Query())
						->select(['pegawai.fname','pegawai.lname','pegawai.username','prestasi.nama_prestasi','prestasi.tahun_prestasi',
							'prestasi.tingkat_prestasi'])
						->from('prestasi')
						->join('INNER JOIN', 'pegawai', 'prestasi.id_pegawai = pegawai.id_pegawai')
						->limit(5)
						->all();

			return $this->render('daftar-prestasi-index', [
					'query'=>$query
				]);
		}
	}
?>
