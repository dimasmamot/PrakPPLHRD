<?php
	namespace app\controllers;

	use Yii;
	use yii\filters\AccessControl;
	use yii\web\Controller;

	class RekapAbsensiController extends Controller{
		public function actions(){
			return[
				'error' => [
					'class' => 'yii\web\ErrorAction',
				],
			];
		}

		public function actionIndex(){
			$query = (new \yii\db\Query())
						->select(['pegawai.fname','pegawai.lname','pegawai.username','absensi.waktu_absen','absensi.status_absen'])
						->from('absensi')
						->join('INNER JOIN', 'pegawai', 'absensi.id_pegawai = pegawai.id_pegawai')
						->limit(5)
						->all();

			return $this->render('query-builder-index', [
					'query'=>$query
				]);
		}
	}
?>
