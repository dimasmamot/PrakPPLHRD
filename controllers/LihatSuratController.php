<?php
	namespace app\controllers;

	use Yii;
	use yii\filters\AccessControl;
	use yii\web\Controller;

	class LihatSuratController extends Controller{
		public function actions(){
			return[
				'error' => [
					'class' => 'yii\web\ErrorAction',
				],
			];
		}

		public function actionIndex(){
			$query = (new \yii\db\Query())
						->select(['id_surat',
							'jenis_surat',
							'tanggal_request',
							'status_acc'])
						->from('surat')
						->where(['id_pegawai' => Yii::$app->user->identity->id_pegawai])
						->limit(5)
						->all();

			return $this->render('lihat-surat-index', [
					'query'=>$query
				]);
		}
	}
?>
