<?php
	namespace app\controllers;

	use Yii;
	use yii\filters\AccessControl;
	use yii\web\Controller;
	use yii\db\QueryBuilder;

	class SubmitAbsensiController extends Controller{
		public function actions(){
			return[
				'error' => [
					'class' => 'yii\web\ErrorAction',
				],
			];
		}

		public function actionIndex(){
			$sql = Yii::$app->db->createCommand()
					->insert('absensi', 
						[
						'id_pegawai' => Yii::$app->user->identity->id_pegawai
					])->execute();

			return $this->render('submit-absensi-index');
		}
	}
?>
