-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2016 at 03:15 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boop`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id_absensi` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `waktu_absen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_absen` char(30) DEFAULT 'Sudah Absen'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `id_pegawai`, `waktu_absen`, `status_absen`) VALUES
(1, 4, '2016-11-10 19:28:15', 'Sudah Absen'),
(2, 2, '2016-11-10 19:28:35', 'Sudah Absen'),
(3, 3, '2016-11-10 23:22:56', 'Belum Absen'),
(4, 3, '2016-11-10 23:23:17', 'Belum Absen');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `code` char(2) NOT NULL,
  `name` char(52) NOT NULL,
  `population` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`code`, `name`, `population`) VALUES
('AU', 'Australia', 18886000),
('BR', 'Brazil', 170115000),
('CA', 'Canada', 1147000),
('CN', 'China', 1277558000),
('DE', 'Germany', 82164700),
('FR', 'France', 59225700),
('GB', 'United Kingdom', 59623400),
('IN', 'India', 1013662000),
('RU', 'Russia', 146934000),
('US', 'United States', 278357000);

-- --------------------------------------------------------

--
-- Table structure for table `lembur`
--

CREATE TABLE `lembur` (
  `id_lembur` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `upah_lembur` int(11) DEFAULT NULL,
  `aktivitas_lembur` char(40) DEFAULT NULL,
  `waktu_lembur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lembur`
--

INSERT INTO `lembur` (`id_lembur`, `id_pegawai`, `upah_lembur`, `aktivitas_lembur`, `waktu_lembur`) VALUES
(1, 3, 400, 'Audit dokumen asuransi pasien', 4);

-- --------------------------------------------------------

--
-- Table structure for table `memo`
--

CREATE TABLE `memo` (
  `id_memo` int(11) NOT NULL,
  `judul_memo` char(40) DEFAULT NULL,
  `isi_memo` text,
  `status_memo` char(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `memo`
--

INSERT INTO `memo` (`id_memo`, `judul_memo`, `isi_memo`, `status_memo`) VALUES
(1, 'Meeting dengan dokter stephen strange', 'segera persiapkan file audit dan data mitra jam 1 di ruang meeting gd barat lt 4', 'Mendesak');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `pass` varchar(50) DEFAULT NULL,
  `authKey` varchar(50) DEFAULT NULL,
  `accessToken` varchar(50) DEFAULT NULL,
  `role` varchar(10) DEFAULT NULL,
  `fname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `no_telpon` varchar(20) DEFAULT NULL,
  `gaji` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `username`, `pass`, `authKey`, `accessToken`, `role`, `fname`, `lname`, `email`, `no_telpon`, `gaji`) VALUES
(1, 'mamot', 'admin', 'mamot-123', 'mamotmot1231041', 'Admin', 'Dimas', 'Rizky', 'dimasrizkyhp@gmail.com', '082314123152', 12000),
(2, 'akise', 'admin', 'akise2131', 'akiseprasi10i399913ff', 'Sekretaris', 'Prasetyo', 'Wibowo', 'prasetyoakise@gmail.com', '083921341598', 1000),
(3, 'bomber', 'pegawai', 'bomber-1391', 'bomb9939aosidas', 'Pegawai', 'Ilzam', 'Fuady', 'ilzamfuady@gmail.com', '08129381294', 800),
(4, 'strange', 'shambala', 'strange-1241', 'mrdrstrange01931dormammuivecometobargain', 'Manajer', 'Stephen', 'Strange', 'dormammubargain@gmail.com', '08129318423', 40000);

-- --------------------------------------------------------

--
-- Table structure for table `posisi`
--

CREATE TABLE `posisi` (
  `id_posisi` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `nama_posisi` char(50) DEFAULT NULL,
  `departemen` char(50) DEFAULT NULL,
  `kota` char(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posisi`
--

INSERT INTO `posisi` (`id_posisi`, `id_pegawai`, `nama_posisi`, `departemen`, `kota`) VALUES
(1, 1, 'Kepala Bagian Pengembangan Unit', 'HRD', 'Jakarta'),
(2, 2, 'Kepala Pusat Pelatihan Pasien Gangguan Mental', 'Kepelatihan', 'Ambon'),
(3, 3, 'Kepala Terapi Game Penyandang Tunanetra', 'Pengembangan Unit Pelayanan', 'Bandung');

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id_prestasi` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `nama_prestasi` varchar(20) DEFAULT NULL,
  `tahun_prestasi` varchar(20) DEFAULT NULL,
  `tingkat_prestasi` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `surat`
--

CREATE TABLE `surat` (
  `id_surat` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `jenis_surat` char(50) DEFAULT NULL,
  `tanggal_request` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_acc` char(20) DEFAULT 'Belum',
  `tanggal_acc` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat`
--

INSERT INTO `surat` (`id_surat`, `id_pegawai`, `jenis_surat`, `tanggal_request`, `status_acc`, `tanggal_acc`) VALUES
(2, 1, 'Surat Izin Cuti', NULL, 'Dikonfirmasi', NULL),
(3, 4, 'Surat Promosi', '2016-11-10 19:04:56', 'Dikonfirmasi', NULL),
(4, 3, 'Surat Ijin', '2016-11-11 01:40:21', 'Dikonfirmasi', NULL),
(5, 3, 'Surat Cuti', '2016-11-11 01:40:39', 'Belum', NULL),
(6, 3, 'Surat Jalan', '2016-11-11 01:40:43', 'Belum', NULL),
(7, 3, 'Surat Keterangan Gaj', '2016-11-11 01:40:47', 'Belum', NULL),
(8, 3, 'Surat Pengunduran Di', '2016-11-11 01:40:52', 'Belum', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id_absensi`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `lembur`
--
ALTER TABLE `lembur`
  ADD PRIMARY KEY (`id_lembur`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `memo`
--
ALTER TABLE `memo`
  ADD PRIMARY KEY (`id_memo`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `posisi`
--
ALTER TABLE `posisi`
  ADD PRIMARY KEY (`id_posisi`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id_prestasi`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`id_surat`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id_absensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `lembur`
--
ALTER TABLE `lembur`
  MODIFY `id_lembur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `memo`
--
ALTER TABLE `memo`
  MODIFY `id_memo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `posisi`
--
ALTER TABLE `posisi`
  MODIFY `id_posisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id_prestasi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `absensi`
--
ALTER TABLE `absensi`
  ADD CONSTRAINT `absensi_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON UPDATE CASCADE;

--
-- Constraints for table `lembur`
--
ALTER TABLE `lembur`
  ADD CONSTRAINT `lembur_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON UPDATE CASCADE;

--
-- Constraints for table `posisi`
--
ALTER TABLE `posisi`
  ADD CONSTRAINT `posisi_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON UPDATE CASCADE;

--
-- Constraints for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD CONSTRAINT `prestasi_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON UPDATE CASCADE;

--
-- Constraints for table `surat`
--
ALTER TABLE `surat`
  ADD CONSTRAINT `surat_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
