<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "memo".
 *
 * @property integer $id_memo
 * @property string $judul_memo
 * @property string $isi_memo
 * @property string $status_memo
 */
class Memo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'memo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isi_memo'], 'string'],
            [['judul_memo', 'status_memo'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_memo' => 'Id Memo',
            'judul_memo' => 'Judul Memo',
            'isi_memo' => 'Isi Memo',
            'status_memo' => 'Status Memo',
        ];
    }
}
