<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pegawai".
 *
 * @property integer $id_pegawai
 * @property string $username
 * @property string $pass
 * @property string $authKey
 * @property string $accessToken
 * @property string $role
 * @property string $fname
 * @property string $lname
 * @property string $email
 * @property string $no_telpon
 * @property integer $gaji
 *
 * @property Absensi[] $absensis
 * @property Lembur[] $lemburs
 * @property Prestasi[] $prestasis
 * @property Surat[] $surats
 */
class Pegawai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pegawai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gaji'], 'integer'],
            [['username', 'email'], 'string', 'max' => 30],
            [['pass', 'authKey', 'accessToken'], 'string', 'max' => 50],
            [['role'], 'string', 'max' => 10],
            [['fname', 'lname', 'no_telpon'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pegawai' => 'Id Pegawai',
            'username' => 'Username',
            'pass' => 'Pass',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'role' => 'Role',
            'fname' => 'Fname',
            'lname' => 'Lname',
            'email' => 'Email',
            'no_telpon' => 'No Telpon',
            'gaji' => 'Gaji',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAbsensis()
    {
        return $this->hasMany(Absensi::className(), ['id_pegawai' => 'id_pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLemburs()
    {
        return $this->hasMany(Lembur::className(), ['id_pegawai' => 'id_pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrestasis()
    {
        return $this->hasMany(Prestasi::className(), ['id_pegawai' => 'id_pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurats()
    {
        return $this->hasMany(Surat::className(), ['id_pegawai' => 'id_pegawai']);
    }
}
