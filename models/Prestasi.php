<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prestasi".
 *
 * @property integer $id_prestasi
 * @property integer $id_pegawai
 * @property string $nama_prestasi
 * @property string $tahun_prestasi
 * @property string $tingkat_prestasi
 *
 * @property Pegawai $idPegawai
 */
class Prestasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prestasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pegawai'], 'required'],
            [['id_pegawai'], 'integer'],
            [['nama_prestasi', 'tahun_prestasi', 'tingkat_prestasi'], 'string', 'max' => 20],
            [['id_pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['id_pegawai' => 'id_pegawai']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_prestasi' => 'Id Prestasi',
            'id_pegawai' => 'Nama Pegawai',
            'nama_prestasi' => 'Nama Prestasi',
            'tahun_prestasi' => 'Tahun Prestasi',
            'tingkat_prestasi' => 'Tingkat Prestasi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPegawai()
    {
        return $this->hasOne(Pegawai::className(), ['id_pegawai' => 'id_pegawai']);
    }
}
