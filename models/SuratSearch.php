<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Surat;

/**
 * SuratSearch represents the model behind the search form about `app\models\Surat`.
 */
class SuratSearch extends Surat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_surat', 'id_pegawai'], 'integer'],
            [['jenis_surat', 'tanggal_request', 'status_acc', 'tanggal_acc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Surat::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_surat' => $this->id_surat,
            'id_pegawai' => $this->id_pegawai,
            'tanggal_request' => $this->tanggal_request,
            'tanggal_acc' => $this->tanggal_acc,
        ]);

        $query->andFilterWhere(['like', 'jenis_surat', $this->jenis_surat])
            ->andFilterWhere(['like', 'status_acc', $this->status_acc]);

        return $dataProvider;
    }
}
