<?php

namespace app\models;
use app\models\Pegawai;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id_pegawai;
    public $username;
    public $pass;
    public $authKey;
    public $accessToken;
    public $role;
    public $fname;
    public $lname;
    public $email;
    public $no_telpon;
    public $gaji;

    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $user = Pegawai::findOne($id); 
        if(count($user)){
            return new static($user);
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = Pegawai::find()->where(['accessToken'=>$token])->one(); 
        if(count($user)){
            return new static($user);
        }
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user = Pegawai::find()->where(['username'=>$username])->one(); 
        if(count($user)){
            return new static($user);
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id_pegawai;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->pass === $password;
    }
}
