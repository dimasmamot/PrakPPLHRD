<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lembur".
 *
 * @property integer $id_lembur
 * @property integer $id_pegawai
 * @property integer $upah_lembur
 * @property string $aktivitas_lembur
 * @property integer $waktu_lembur
 *
 * @property Pegawai $idPegawai
 */
class Lembur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lembur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pegawai'], 'required'],
            [['id_pegawai', 'upah_lembur', 'waktu_lembur'], 'integer'],
            [['aktivitas_lembur'], 'string', 'max' => 40],
            [['id_pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['id_pegawai' => 'id_pegawai']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_lembur' => 'Id Lembur',
            'id_pegawai' => 'Nama Pegawai',
            'upah_lembur' => 'Upah Lembur',
            'aktivitas_lembur' => 'Aktivitas Lembur',
            'waktu_lembur' => 'Waktu Lembur',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPegawai()
    {
        return $this->hasOne(Pegawai::className(), ['id_pegawai' => 'id_pegawai']);
    }
}
