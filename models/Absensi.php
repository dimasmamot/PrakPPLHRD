<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "absensi".
 *
 * @property integer $id_absensi
 * @property integer $id_pegawai
 * @property string $waktu_absen
 * @property string $status_absen
 *
 * @property Pegawai $idPegawai
 */
class Absensi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'absensi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pegawai'], 'required'],
            [['id_pegawai'], 'integer'],
            [['waktu_absen'], 'safe'],
            [['status_absen'], 'string', 'max' => 30],
            [['id_pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['id_pegawai' => 'id_pegawai']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_absensi' => 'Id Absensi',
            'id_pegawai' => 'Id Pegawai',
            'waktu_absen' => 'Waktu Absen',
            'status_absen' => 'Status Absen',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPegawai()
    {
        return $this->hasOne(Pegawai::className(), ['id_pegawai' => 'id_pegawai']);
    }
}
