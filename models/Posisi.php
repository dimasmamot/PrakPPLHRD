<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posisi".
 *
 * @property integer $id_posisi
 * @property integer $id_pegawai
 * @property string $nama_posisi
 * @property string $departemen
 * @property string $kota
 *
 * @property Pegawai $idPegawai
 */
class Posisi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posisi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pegawai'], 'integer'],
            [['nama_posisi', 'departemen'], 'string', 'max' => 50],
            [['kota'], 'string', 'max' => 20],
            [['id_pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['id_pegawai' => 'id_pegawai']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_posisi' => 'Id Posisi',
            'id_pegawai' => 'Nama Pegawai',
            'nama_posisi' => 'Nama Posisi',
            'departemen' => 'Departemen',
            'kota' => 'Kota',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPegawai()
    {
        return $this->hasOne(Pegawai::className(), ['id_pegawai' => 'id_pegawai']);
    }
}
