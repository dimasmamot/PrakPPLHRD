<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "surat".
 *
 * @property integer $id_surat
 * @property integer $id_pegawai
 * @property string $jenis_surat
 * @property string $tanggal_request
 * @property string $status_acc
 * @property string $tanggal_acc
 *
 * @property Pegawai $idPegawai
 */
class Surat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'surat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pegawai'], 'required'],
            [['id_pegawai'], 'integer'],
            [['tanggal_request', 'tanggal_acc'], 'safe'],
            [['jenis_surat', 'status_acc'], 'string', 'max' => 20],
            [['id_pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['id_pegawai' => 'id_pegawai']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_surat' => 'Id Surat',
            'id_pegawai' => 'Nama Pegawai',
            'jenis_surat' => 'Jenis Surat',
            'tanggal_request' => 'Tanggal Request',
            'status_acc' => 'Status Acc',
            'tanggal_acc' => 'Tanggal Acc',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPegawai()
    {
        return $this->hasOne(Pegawai::className(), ['id_pegawai' => 'id_pegawai']);
    }
}
